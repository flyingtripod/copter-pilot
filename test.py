import time
from copterboard import CopterBoard


def test1():
    board = CopterBoard()
    board.transmit_on()

    board.set_light(False)
    time.sleep(1)
    board.set_light(True)
    time.sleep(1)

    board.transmit_off()


def test2():
    board = CopterBoard()
    board.transmit_on()

    board.set_gaz(0.85)
    time.sleep(7)

    rot = 0.6
    rot_time = 0.4
    board.set_pitch(rot)
    time.sleep(rot_time)
    board.set_roll(rot)
    time.sleep(rot_time)
    board.set_pitch(-rot)
    time.sleep(rot_time)
    board.set_roll(-rot)
    time.sleep(rot_time)
    board.set_pitch(rot)
    time.sleep(rot_time)

    board.set_pitch(0)
    board.set_roll(0)
    time.sleep(0.5)

    board.set_gaz(0.5)
    time.sleep(4)

    board.set_gaz(0)
    time.sleep(0.2)
    board.transmit_off()


def test3():
    board = CopterBoard()
    board.transmit_on()

    board.set_roll(0.1)

    board.set_gaz(1)
    time.sleep(0.5)
    board.set_gaz(0.5)
    time.sleep(0.5)
    board.set_gaz(0.4)
    time.sleep(0.5)

    board.set_gaz(0)
    time.sleep(0.2)
    board.transmit_off()


def test4():
    board = CopterBoard()
    board.transmit_on()

    board.set_gaz(1)
    time.sleep(5)

    board.set_gaz(0)
    time.sleep(0.2)
    board.transmit_off()


test2()
