from copterboard import CopterBoard

import time
import RPi.GPIO as GPIO

GPIO_TRIGGER = 5
GPIO_ECHO = 6

def setup_hc():
    GPIO.setmode(GPIO.BCM)

    GPIO.setup(GPIO_TRIGGER, GPIO.OUT)  # Trigger
    GPIO.setup(GPIO_ECHO, GPIO.IN)  # Echo

    GPIO.output(GPIO_TRIGGER, False)

    time.sleep(0.5)

def get_dist():
    GPIO.output(GPIO_TRIGGER, True)
    time.sleep(0.00001)
    GPIO.output(GPIO_TRIGGER, False)
    start = time.time()
    stop = 0
    sst = time.time()
    while GPIO.input(GPIO_ECHO) == 0 and (time.time() - sst) < 1:
        start = time.time()

    while GPIO.input(GPIO_ECHO) == 1 and (time.time() - start) < 1:
        stop = time.time()

    elapsed = stop - start

    distance = elapsed * 34000
    distance /= 2

    return distance


def start(hh):
    setup_hc()
    board = CopterBoard()
    board.set_gaz(0)

    start = time.time()
    board.transmit_on()
    H = 0
    gaz = 0
    while H < hh and (time.time() - start) < 10:
        dist = get_dist()
        if dist < 0:
            continue
        print "Dist:", dist
        if dist < hh:
            gaz += 0.05
        else:
            gaz -= 0.05
        board.set_gaz(gaz)

        H = dist

    board.set_gaz(0.4)
    time.sleep(3)

    board.transmit_off()

start(20)
