import wiringpi2 as wiringpi

CMD_SET_TRANSMIT = 0x11
CMD_SET_TRANSMIT_DATA = 0x12
CMD_NONE = 0x7F

TRANSMIT_DATA_LEN = 16


class CopterBoard:
    def __init__(self):
        wiringpi.wiringPiSetup()
        self._serial = wiringpi.serialOpen('/dev/ttyAMA0', 115200)

    def __exit__(self, *args, **kwargs):
        wiringpi.serialClose(self._serial)

    def transmit_on(self):
        self._set_transmit(True)

    def transmit_off(self):
        self._set_transmit(False)

    def set_gaz(self, value):
        self._gaz = value
        self._update_data()

    def set_yaw(self, value):
        self._yaw = value
        self._update_data()

    def set_pitch(self, value):
        self._pitch = value
        self._update_data()

    def set_roll(self, value):
        self._roll = value
        self._update_data()

    def set_light(self, value):
        self._light = value
        self._update_data()

    def _update_data(self):
        data = self._get_transmit_data()
        self._set_transmit_data(data + [self._get_check_sum(data)])

    def _get_transmit_data(self):
        state_bits = {
            0: self._function1,
            1: self._function2,
            2: self._360flip,
            4: not self._light
        }
        state = reduce(lambda a, b: a | _bit(b, state_bits[b]), state_bits)

        return [
            _to_byte(self._gaz),
            _to_sign_byte(self._yaw),
            _to_sign_byte(self._pitch),
            _to_sign_byte(self._roll),
            self._yaw_calibrate,
            self._pitch_calibrate,
            self._roll_calibrate,
            0x1A, 0x26,
            0, 0, 0, 0, 0,
            state
        ]

    @staticmethod
    def _get_check_sum(data):
        return reduce(lambda a, b: (a + b) % 0x100, data)

    def _set_transmit(self, transmit):
        self._send_bytes([CMD_SET_TRANSMIT, 1 if transmit else 0])

    def _set_transmit_data(self, transmit_data):
        if len(transmit_data) != TRANSMIT_DATA_LEN:
            raise ValueError('Transmit data must have len = ' + str(TRANSMIT_DATA_LEN))

        print 'Set data:', ', '.join(hex(x) for x in transmit_data)

        self._send_bytes([CMD_SET_TRANSMIT_DATA] + transmit_data)

    def _send_bytes(self, data):
        for x in data:
            wiringpi.serialPutchar(self._serial, x)

    _gaz = 0
    _yaw = 0  # rotating in land plate
    _pitch = 0  # forward / back
    _roll = 0  # right / left

    _yaw_calibrate = 64
    _pitch_calibrate = 64
    _roll_calibrate = 64

    _function1 = False
    _function2 = False
    _360flip = False
    _light = True


def _to_byte(value):
    return int(value * 0xFF)


def _to_sign_byte(value):
    byte = int((value + 1.0) / 2 * 0xFF)
    return byte if byte & 0x80 else byte ^ 0x7F


def _revert_minus(value):
    return value ^ 0x80


def _bit(bit, value):
    return 1 << bit if value else 0
