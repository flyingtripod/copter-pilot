import wiringpi2 as wiringpi

wiringpi.wiringPiSetup()
serial = wiringpi.serialOpen('/dev/ttyAMA0', 115200)
wiringpi.serialPuts(serial, "Hello")
for _ in range(5):
    print chr(wiringpi.serialGetchar(serial))